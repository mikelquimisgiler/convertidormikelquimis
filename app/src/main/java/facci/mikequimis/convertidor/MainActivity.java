package facci.mikequimis.convertidor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button buttonF;
    Button buttonC;
    EditText EditTextValor;
    TextView textViewresultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonF = (Button) findViewById(R.id.buttonF);
        buttonC = (Button) findViewById(R.id.buttonC);
        EditTextValor = (EditText) findViewById(R.id.EditTextValor);
        textViewresultado = (TextView) findViewById(R.id.Textviewresultado);

        buttonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Se toma el valor ingresado y se realiza la conversion
                int numeroingresado = Integer.parseInt(EditTextValor.getText().toString());
                float conversion = (float) ((numeroingresado * 1.8) + 32);
                textViewresultado.setText(String.valueOf(conversion+"°F"));
            }
        });

        buttonF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Se toma el valor ingresado y se realiza la conversion
                int numeroingresado = Integer.parseInt(EditTextValor.getText().toString());
                float conversion = (float) ((numeroingresado - 32)/(1.8));
                textViewresultado.setText(String.valueOf(conversion+"°C"));
            }
        });


    }
}
